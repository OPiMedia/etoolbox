#!/bin/bash

#
# Usage: 256-colors-side.sh [OPTION]... [COLOR]
#
# Displays side by side two tables of 256 ANSI colors in your terminal.
# For each COLOR number, displays also a line with the escape code to use.
#
# Options:
#   --skip-tables   doesn't show the two tables
#   --text STRING   text example displayed for each COLOR (default: " Text 123 ")
#   -h, --help      display this help text and exit
#
# Read these explanations:
# "Bash tips: Colors and formatting (ANSI/VT100 Control sequences)" (Fabien Loison)
# https://misc.flogisoft.com/bash/tip_colors_and_formatting
#
# "Terminals compatibility":
# https://misc.flogisoft.com/bash/tip_colors_and_formatting#terminals_compatibility
#
# GPLv3 --- Copyright (C) 2023 Olivier Pirson
# http://www.opimedia.be/
##
# * November 19, 2023: Few cleaning.
# * November 12, 2023:
#     - Fixed background bug in example lines.
#     - Protected last end of line in --text argument.
# * November 9, 2023: First improved version.
# * November 13, 2021: Cleaned ${#right[*]} expansion.
# * Started August 17, 2020
#
# The origin of this script is a modified version of "256-colors.sh" written by Fabien Loison:
# https://misc.flogisoft.com/_export/code/bash/tip_colors_and_formatting?codeblock=56
#

#
# Original licence, for Fabien Loison script "256-colors.sh":
#
# This program is free software. It comes without any warranty, to
# the extent permitted by applicable law. You can redistribute it
# and/or modify it under the terms of the Do What The Fuck You Want
# To Public License, Version 2, as published by Sam Hocevar. See
# https://web.archive.org/web/20231030043507/http://www.wtfpl.net/txt/copying/
#

TEXT=' Text 123 '


# Displays the ANSI code to set the foreground or background color to number, for 256 colors mode
# If $2 == '0', then sets background color,
# else sets foreground color.
function color_256 {  # number [is_background]
    local -i -r number="$1"
    local -r is_background="$2"

    if [[ "$is_background" == '0' ]]; then
        color_256_background "${number}"
    else
        color_256_foreground "${number}"
    fi
}


# Displays the ANSI code to set the background color to number, for 256 colors mode
function color_256_background {  # number
    local -i -r number="$1"

    printf '%s' "\e[48;5;${number}m"
}


# Displays the ANSI code to set the foreground color to number, for 256 colors mode
function color_256_foreground {  # number
    local -i -r number="$1"

    printf '%s' "\e[38;5;${number}m"
}


# Displays the ANSI code to reset all attributes
function color_reset {
    printf '\\e[0m'
}


# Displays a example line for each argument COLOR
function colors_list {  # [color1 ...]
    local foreground
    local background
    local code_foreground
    local code_background

    local color
    for color in "$@"; do
        IFS=':' read -r foreground background <<< "$color"
        if [[ -z $foreground ]]; then
            code_foreground=''
        else
            code_foreground=$(color_256 "$foreground")
        fi
        if [[ -z $background ]]; then
            code_background=''
        else
            code_background=$(color_256 "$background" 0)
        fi
        printf '%3s:%3s   %12s%-12s %b%b%s%b %s\n' "$foreground" "$background" "$code_foreground" "$code_background" "$code_foreground" "$code_background" "$TEXT" "$(color_reset)" "$(color_reset)"
    done
}  # colors_list


# Displays the two tables
function colors_tables {
    local -a left
    local -a right

    local s

    # Builds tables left (for foreground) and right (for background)
    local -i is_background
    for is_background in 1 0 ; do  # foreground / background
        local line='          '

        local -i color
        for color in {0..255} ; do
            # Display the color
            s=$(printf '%b %3i %b' "$(color_256 "$color" $is_background)" "$color" "$(color_reset)")
            line+="$s"

            # Display 6 colors per lines (in one table)
            if [ $(((color + 1) % 6)) -eq 4 ] ; then
                if [ $is_background -eq 1 ]; then
                    left+=("$line")
                else
                    right+=("$line")
                fi
                line=''
            fi
        done
    done

    # Displays tables
    local -i nb="${#right[@]}"
    nb+=-1
    for i in $(seq 0 "$nb"); do
        printf '%s   %s\n' "${left[$i]}" "${right[$i]}"
    done
}  # colors_tables


function main {  # [argument1 ...]
    # Prints text_in_red and text in the error output.
    function error_msg {  # text_in_red, text
        echo "$(tput setaf 1)$1$(tput sgr0) $2" 1>&2
    }

    # Prints "$2", the next argument given, following by an ending character 'E'.
    # The purpose is to protect trailing newlines when this function is used inside $(...),
    # else the evaluation removes them.
    function next_argument_E {  # [argument1 ...]
        if [[ $# -lt 2 ]]; then  # missing argument
            print_help_exit "$1"
        fi
        printf '%sE' "$2"
    }

    # Prints help message (reads from the script itself) and quits.
    # If called without argument,
    # then prints to stdout and exit 0,
    # else prints to stderr and exit 1.
    function print_help_exit {  # [wrong option]
        local -r wrong_option="$1"
        local -i return_code=0

        if [[ $# -ne 0 ]]; then
            return_code=1
            exec 1>&2
            error_msg 'Unknown or wrong option!' "\"$wrong_option\""
        fi

        local -i skip_begin=0

        local line
        while IFS='' read -r line; do  # prints the header comment until the line ##
            if [[ "$line" == '# Usage:'* ]]; then skip_begin=1; fi
            if [[ "$skip_begin" -eq 0 ]]; then continue; fi
            if [[ "$line" == '##'* ]]; then break; fi
            line="${line:2}"
            printf '%s\n' "${line//proc_deleted/$script_name}"
        done < "${BASH_SOURCE[0]}"

        echo "Current version: $(print_version)"

        exit $return_code
    }

    # Prints version (reads it from the script itself, just after the first ## line).
    function print_version {
        local -i skip_begin=0
        local version

        local line
        while IFS='' read -r line; do
            if [[ "$line" == '##'* ]]; then skip_begin=1; fi
            if [[ "$skip_begin" -eq 0 ]]; then continue; fi
            line="${line:4}"
            if [[ -n "$line" ]]; then
                IFS=':' read -r version line <<< "$line"
                printf '%s\n' "$version"

                break
            fi
        done < "${BASH_SOURCE[0]}"
    }


    #
    # Reads arguments and set options
    #
    local -a args
    local -i is_show_tables=0  # 0 is for true, other is for false

    while [[ $# -ne 0 ]]; do
        case "$1" in
            -h|--help)
                print_help_exit
                ;;
            --skip-tables)
                is_show_tables=1
                ;;
            --text)
                TEXT=$(next_argument_E "$@") || exit $?
                TEXT="${TEXT%E}"  # remove guard E used to keep possible ending lines
                shift
                ;;
            -*)  # unknown argument
                print_help_exit "$1"
                ;;
            *)  # considered as a COLOR argument
                args+=("$1")
                ;;
        esac
        shift
    done  # [[ $# -ne 0 ]]

    if [[ is_show_tables -eq 0 ]]; then
        colors_tables
        if [[ ${#args[@]} -ne 0 ]]; then
            echo
        fi
    fi

    colors_list "${args[@]}"
}  # main

main "$@"

exit 0
