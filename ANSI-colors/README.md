# ANSI colors
* [`256-colors-side.sh`](https://bitbucket.org/OPiMedia/etoolbox/src/master/ANSI-colors/256-colors-side.sh):
  Display side by side two tables of 256 colors in your terminal.

![256-colors-side.png](https://bitbucket.org/OPiMedia/etoolbox/raw/master/ANSI-colors/_img/256-colors-side.png)



## References
* [_ANSI escape code_](https://en.wikipedia.org/wiki/ANSI_escape_code) (Wikipedia)
* [_Bash tips: Colors and formatting (ANSI/VT100 Control sequences)_](https://misc.flogisoft.com/bash/tip_colors_and_formatting) (Fabien Loison):
  [_Terminals compatibility_](https://misc.flogisoft.com/bash/tip_colors_and_formatting#terminals_compatibility)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Bluesky: <https://bsky.app/profile/opimedia.bsky.social> — Mastodon: <https://mamot.fr/@OPiMedia> — X/Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)
Copyright (C) 2023 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



## Changes
* November 19, 2023: Few cleaning.
* November 12, 2023:

    - Fixed background bug in example lines.
    - Protected last end of line in `--text` argument.

* November 9, 2023: First improved version.
* November 13, 2021: Cleaned `${#right[*]}` expansion.
* Started August 17, 2020
