#!/bin/bash

#
# Usage: source set_example_full.sh
#
# Sets shprompt to have the full example
# showed in the shprompt--full*.png screenshots.
#
# December 29, 2023
#
# Apache License 2.0 --- Copyright (C) 2023 Olivier Pirson
# http://www.opimedia.be/
#

if [[ ! -e ../../shprompt_install.sh ]]; then
    echo 'Move first to the directory contained "shprompt_install.sh".'

    return 1
fi

. ../../shprompt_install.sh

USER='user'

export PATH="$PWD:$PATH"  # to take the fake hostname command in this directory

cd ~/Documents || exit 1
