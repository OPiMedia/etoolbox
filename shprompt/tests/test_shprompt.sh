#!/bin/bash

#
# Usage: test_shprompt.sh
#
# December 29, 2023
#
# Apache License 2.0 --- Copyright (C) 2023 Olivier Pirson
# http://www.opimedia.be/
#

OUTPUT="$PWD/test_shprompt_output.txt"

{  # Write stdout and stderr to "$OUTPUT"
    echo "Test results of shprompt --- $(date)"


    # ShellCheck
    for SHELL in sh ksh bash; do
        echo "\$ shellcheck --shell $SHELL ../shprompt_install.sh"
        shellcheck --shell "$SHELL" ../shprompt_install.sh
    done

    echo '$ shellcheck test_shprompt.sh'
    shellcheck test_shprompt.sh

    (
        cd examples || exit 1
        for FILE in hostname set_example_*.sh; do
            echo "\$ shellcheck $FILE"
            shellcheck --external-sources "$FILE"
        done
    )


    # Source the script in each possible shell
    for SHELL in sh ksh bash zsh; do
        echo "----- $SHELL - script sourced -----"
        echo '. ../shprompt_install.sh' | $SHELL
    done

    echo '---------- END ----------'
} 2>&1 | tee "$OUTPUT"


# Compare results
echo "COMPARE $OUTPUT and test_shprompt_correct_output.txt"
if diff --color <(tail --lines +2 "$OUTPUT") <(tail --lines +2 test_shprompt_correct_output.txt); then
    echo "$(tput setaf 2)OK$(tput sgr0)"
else
    echo "$(tput setaf 1)DIFFERENT$(tput sgr0)"
fi
