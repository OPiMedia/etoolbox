#
# Usage: source shprompt_install.sh [USER_NAME [HOST_NAME [DOMAIN_NAME [SHELL_NAME]]]]
# Or for sh: . shprompt_install.sh
#
# Source this script in your shell to define a fancy PS1 prompt.
# That works at least with sh, ksh, bash and zsh.
#
# For sh:
# . <DIR>/shprompt_install.sh
#
# For ksh, bash and zsh:
# source <DIR>/shprompt_install.sh [USER_NAME [HOST_NAME [DOMAIN_NAME [SHELL_NAME]]]]
#
# If you specify USER_NAME, HOST_NAME, DOMAIN_NAME or SHELL_NAME
# the corresponding parts are not displayed when they are equal to the given values.
#
# Requirements: hostname, mktemp, rm
#
# Latest version and more explanations on Bitbucket:
# https://bitbucket.org/OPiMedia/etoolbox/src/master/shprompt/
#
# Apache License 2.0 --- Copyright (C) 2023 Olivier Pirson
# http://www.opimedia.be/
##
# * December 29, 2023: First public version.
# * Started December 27, 2023.
#

#
# Color constants
# https://en.wikipedia.org/wiki/ANSI_escape_code#SGR_(Select_Graphic_Rendition)_parameters
# \e syntax doesn't work with ksh
#
SHPROMPT__CHROOT_ANSI='\033[33m'

SHPROMPT__USER_ANSI='\033[1m\033[32m'
SHPROMPT__ROOT_ANSI='\033[1m\033[31m'

SHPROMPT__HOST_ANSI='\033[3m\033[32m'
SHPROMPT__DOMAIN_ANSI='\033[32m'

SHPROMPT__DIRECTORY_ANSI="\033[1m\033[94m"

SHPROMPT__SHELL_ANSI='\033[33m'

SHPROMPT__RETURN_ERROR_CODE_ANSI='\033[41m'


SHPROMPT__RESET_ANSI='\033[0m'



#
# Identify which shell is used
#
if [ -n "${BASH_VERSION}" ]; then
    __SHPROMPT__SHELL='bash'
elif [ -n "${ZSH_VERSION}" ]; then
    __SHPROMPT__SHELL='zsh'
    setopt prompt_subst
elif [ -n "${KSH_VERSION}" ]; then
    __SHPROMPT__SHELL='ksh'
elif [ -n "${SH_VERSION}" ]; then
    __SHPROMPT__SHELL='sh'
else
    __SHPROMPT__SHELL=$(basename "$(readlink /proc/$$/exe)")
fi



#
# Temporary file to save the last return code
#
__SHPROMPT__LAST_RETURN_CODE_FILENAME=$(mktemp -t shprompt__last_return_code.XXXXXXXXXX)
# shellcheck disable=SC2064  # $__SHPROMPT__LAST_RETURN_CODE_FILENAME needs to be evaluate directly
trap "rm --force '$__SHPROMPT__LAST_RETURN_CODE_FILENAME'" EXIT



#
# Displaying functions for all parts
#
shprompt__save_last_return_code() {
    echo "$?" > "$__SHPROMPT__LAST_RETURN_CODE_FILENAME"
}

shprompt__chroot() {
    if [ -n "$debian_chroot" ]; then
        printf '%b(%s)%b' "$SHPROMPT__CHROOT_ANSI" "$debian_chroot" "$SHPROMPT__RESET_ANSI"
    fi
}

shprompt__user() {  # [user]
    if [ "$USER" = root ]; then
        printf '%broot%b@' "$SHPROMPT__ROOT_ANSI" "$SHPROMPT__RESET_ANSI"
    elif [ -z "$1" ] || [ "$USER" != "$1" ]; then
        printf '%b%s%b@' "$SHPROMPT__USER_ANSI" "$USER" "$SHPROMPT__RESET_ANSI"
    fi
}

shprompt__host() {  # [host]
    __shprompt__hostname=$(hostname --short)

    if [ -z "$1" ] || [ "$__shprompt__hostname" != "$1" ]; then
        printf '%b%s%b.' "$SHPROMPT__HOST_ANSI" "$__shprompt__hostname" "$SHPROMPT__RESET_ANSI"
    fi
}

shprompt__domain() {  # [domain]
    __shprompt__domainname=$(hostname --domain)

    if [ -z "$1" ] || [ "$__shprompt__domainname" != "$1" ]; then
        printf '%b%s%b:' "$SHPROMPT__DOMAIN_ANSI" "$__shprompt__domainname" "$SHPROMPT__RESET_ANSI"
    fi
}

shprompt__directory() {
    case "$PWD" in
        "$HOME")
            __shprompt__directory='~'
            ;;
        "$HOME/"*)
            __shprompt__directory="~${PWD#"$HOME"}"
            ;;
        *)
            __shprompt__directory="$PWD"
            ;;
    esac
    printf '%b%s%b' "$SHPROMPT__DIRECTORY_ANSI" "$__shprompt__directory" "$SHPROMPT__RESET_ANSI"
}

shprompt__last_return_code() {
    if [ -r "$__SHPROMPT__LAST_RETURN_CODE_FILENAME" ]; then
        read -r __shprompt__last_return_code < "$__SHPROMPT__LAST_RETURN_CODE_FILENAME"
    else
        __shprompt__last_return_code='?'
    fi
    if [ "$__shprompt__last_return_code" != '0' ]; then
        printf '%b%s%b' "$SHPROMPT__RETURN_ERROR_CODE_ANSI" "$__shprompt__last_return_code" "$SHPROMPT__RESET_ANSI"
    fi
}

shprompt__shell() {  # [shell]
    if [ -z "$1" ] || [ "$__SHPROMPT__SHELL" != "$1" ]; then
        printf '%b[%s]%b' "$SHPROMPT__SHELL_ANSI" "$__SHPROMPT__SHELL" "$SHPROMPT__RESET_ANSI"
    fi
}



#
# Main definition
#
PS1="\$(shprompt__save_last_return_code)\$(shprompt__chroot)\$(shprompt__user '$1')\$(shprompt__host '$2')\$(shprompt__domain '$3')\$(shprompt__directory)\$(shprompt__shell '$4')\$(shprompt__last_return_code)\\\$ "
