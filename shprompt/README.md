# shprompt
Defines a fancy `PS1` prompt for `sh`, `ksh`, `bash` and `zsh`:

`user@host.domain:directory[shell]$ `

If the last command failed, also displays the return code:

`user@host.domain:directory[shell]code$ `

![shprompt - example full](https://bitbucket.org/OPiMedia/etoolbox/raw/master/shprompt/_img/screenshots/shprompt--full.png)

If `$debian_chroot` is not empty, then displays it at the beginning:

`(chroot)user@host.domain:directory[shell]$ `

Some parts are conditional. See below.



## Installation
Do the following in your `.shrc`, `.kshrc`, `.bashrc` or `.zshrc` configuration files,
depending of your shell,
to permanently install shprompt.


### For `sh`
```sh
. <DIR>/shprompt_install.sh
```


### For `ksh`, `bash` and `zsh`
```bash
source <DIR>/shprompt_install.sh [USER_NAME [HOST_NAME [DOMAIN_NAME [SHELL_NAME]]]]
```

If you specify `USER_NAME`, `HOST_NAME`, `DOMAIN_NAME` or `SHELL_NAME`
the corresponding parts are not displayed when they are equal to the given values.

For example:

```bash
source <DIR>/shprompt_install.sh 'user' 'host' 'domain' 'bash'
```

Displays the user name, the host name, the domain name and the shell name
only if they are different than `'user'`, `'host'`, `'domain` and `'bash'`.

![shprompt - example short](https://bitbucket.org/OPiMedia/etoolbox/raw/master/shprompt/_img/screenshots/shprompt--short.png)



## Customization
By default `shprompt_install.sh` defines `PS1` like this:

```bash
PS1='$(shprompt__save_last_return_code)$(shprompt__chroot)$(shprompt__user)$(shprompt__host)$(shprompt__domain)$(shprompt__directory)$(shprompt__shell)$(shprompt__last_return_code)\$ '
```

You can change it afterwards by using `shprompt__*` functions.

You can also change colors by overloading `SHPROMPT__CHROOT_ANSI` environment variables.
See the script.



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Bluesky: <https://bsky.app/profile/opimedia.bsky.social> — Mastodon: <https://mamot.fr/@OPiMedia> — X/Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
Copyright 2023 Olivier Pirson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



## Changes
* December 29, 2023: First public version.
* Started December 27, 2023

#shprompt #prompt #PS1 #shell #sh #ksh #bash #zsh
