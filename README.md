# etoolbox
Some various little scripts.

* [ANSI colors](https://bitbucket.org/OPiMedia/etoolbox/src/master/ANSI-colors/):
  `256-colors-side.sh`
* [`finished.sh`](https://bitbucket.org/OPiMedia/etoolbox/src/master/finished/):
  sends a notification
* [`printargs`](https://bitbucket.org/OPiMedia/etoolbox/src/master/printargs/):
  just prints all its arguments
* [`shprompt`](https://bitbucket.org/OPiMedia/etoolbox/src/master/shprompt/):
  define a fancy `PS1` prompt



## See also

* [gocd (Bash)](https://bitbucket.org/OPiMedia/gocd-bash/):
  Changes working directory from a list of association `NAME: DIRECTORY`, with autocompletion.
* [HackerRank [CodinGame…] / helpers](https://bitbucket.org/OPiMedia/hackerrank-codingame-helpers/):
  Helps in solving problems of HackerRank website (or CodinGame…), in several programming languages.
* [make_help](https://bitbucket.org/OPiMedia/make_help/):
  Prints the list of Makefile's targets with descriptions.
* [proc_deleted (Bash)](https://bitbucket.org/OPiMedia/proc_deleted-bash/):
  Lists processes that have some deleted files, and allows to kill them.
* [vNu-scripts](https://bitbucket.org/OPiMedia/vnu-scripts/):
  Shell scripts to run *the Nu Html Checker (v.Nu)* to valid (X)HTML files by identifying if each file is a (X)HTML file and which version of (X)HTML.



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Bluesky: <https://bsky.app/profile/opimedia.bsky.social> — Mastodon: <https://mamot.fr/@OPiMedia> — X/Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



![etoolbox](https://bitbucket-assetroot.s3.amazonaws.com/c/photos/2020/Aug/17/2406260829-1-etoolbox-logo_avatar.png)

([References](https://bitbucket.org/OPiMedia/etoolbox/src/master/_img/_src/_original/)
of images used to create logo)
