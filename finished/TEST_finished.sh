#!/bin/bash

#
# Runs several times ./finished.sh with some options to check it.
#
# November 12, 2023
#
# GPLv3 --- Copyright (C) 2021, 2023 Olivier Pirson
# http://www.opimedia.be/
#

OUTPUT="TEST_finished_output.txt"

{  # Write stdout and stderr to "$OUTPUT"
    echo "Test results of finished.sh --- $(date)"


    for FILE in finished.sh TEST_finished.sh; do
        echo '----------'
        echo "$ shellcheck $FILE"
        shellcheck "$FILE"
    done


    declare -a OPTIONS

    for LINE in \
        '--version' \
            '--help' \
            \
            '---debug-options --no-config --transient' \
            '---debug-options --no-config' \
            '---debug-options --no-config --date' \
            "---debug-options Beginning --no-config --summary SuMmArY second \\ composite\\ \\ \\ next\\  end final" \
            '---debug-options --no-config --return 0' \
            '---debug-options --no-config --return 42' \
        ; do
        # shellcheck disable=SC2162
        IFS=' ' read -a OPTIONS <<< "$LINE"

        echo '----------'
        echo '$ ./finished.sh' "${OPTIONS[@]}"

        # Really runs the command
        ./finished.sh "${OPTIONS[@]}"
    done

    echo '---------- END ----------'
} 2>&1 | tee "$OUTPUT"


# Clean "noise" in the output result
sed -E --in-place \
    -e "s/\[FINISHED [^]]+\]/[FINISHED fake_date_time]/g" \
    "$OUTPUT"


# Compare results
echo "COMPARE $OUTPUT and TEST_finished_correct_output.txt"
if diff --color <(tail --lines +2 "$OUTPUT") <(tail --lines +2 TEST_finished_correct_output.txt); then
    echo "$(tput setaf 2)OK$(tput sgr0)"
else
    echo "$(tput setaf 1)DIFFERENT$(tput sgr0)"
fi
