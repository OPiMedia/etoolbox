# finished.sh
Bash command to send a notification message (via `notify-send`).

The main usage is to send a notification after some other long running process is finished.


## Examples
![finished](https://bitbucket.org/OPiMedia/etoolbox/raw/master/finished/_img/screenshots/finished-sh-Message-to-display.png)

```bash
$ finished.sh Message to display
```

```bash
$ finished.sh "Message: $PWD"
```

```bash
$ sleep 3; finished.sh --sound message-new-email --return $? Message
```

```bash
$ wrong; finished.sh --sound message-new-email --return $? Message
```



## Tips
Define this alias to transmit the return code from the previous command:
```bash
$ alias finished='finished.sh --return $?'
```

And then use it like that:
```bash
$ sleep 3; finished Message
$ wrong; finished Message
```

![wrong](https://bitbucket.org/OPiMedia/etoolbox/raw/master/finished/_img/screenshots/wrong-finished-Message.png)

To show use and options do:

```bash
$ finished.sh --help
$ finished --help
```


## Configuration files
Variables configuration are read from following files (if they exist), in this order:
`"$HOME/.finishedrc"`, `"$HOME/.config/finished/.finishedrc"` and `"./.finishedrc"`.

See example in
[`.finished.config`](https://bitbucket.org/OPiMedia/etoolbox/src/master/finished/.config/finished/.finished.config)

```bash
$ finished.sh Message to display
```

![finished-gray](https://bitbucket.org/OPiMedia/etoolbox/raw/master/finished/_img/screenshots/finished-sh-Message-to-display-gray.png)



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Bluesky: <https://bsky.app/profile/opimedia.bsky.social> — Mastodon: <https://mamot.fr/@OPiMedia> — X/Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [GPLv3](https://www.gnu.org/licenses/gpl-3.0.html) ![GPLv3](https://www.gnu.org/graphics/gplv3-88x31.png)

Copyright (C) 2021, 2023 Olivier Pirson

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program. If not, see <https://www.gnu.org/licenses/>.



## Changes

* November 19, 2023: Few cleaning.
* November 12, 2023: Few cleaning.
* November 14, 2021: Cleaned `IFS`.
* November 4, 2021: Corrected typos.
* October 31, 2021:

    - Added --transient and ---debug-options options.
    - Encapsulated local variables and function to main function.

* October 30, 2021: Set global constants as read only.
* October 27, 2021: Added --i-ok and --sound-ok options.
* Started October 23, 2021: First public version.

#finishedSh
