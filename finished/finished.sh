#!/bin/bash

#
# Usage: finished.sh [--no-config] [OPTION]... [MESSAGE]...
#
# Send a notification with a message (via notify-send).
# The main usage is to send a notification
# after some other long running process is finished.
#
# notify-send options:
#   -i ICON    set icon filename or stock icon to display
#   -t TIME    set timeout in milliseconds at which to expire the notification
#   -u LEVEL   set urgency level (low, normal, critical)
#
# Additional options:
#   --beep              play a beep sound when the notification is sent
#   --date              add display date and time to the summary
#   --i-err     N       set icon used when a failing return code is specified
#   --i-ok      N       set icon used when a 0 return code is specified
#   --no-config         doesn't read configuration files
#   --return CODE       set return code
#   --summary TEXT      set the summary to display (default: "FINISHED")
#   --sound SOUND       set sound filename to play
#   --sound-err SOUND   set sound used when a failing return code is specified
#   --sound-ok SOUND    set sound used when a 0 return code is specified
#   --transient         doesn't keep read notification in history
#
# Help options:
#   ---debug-options   prints command line options to stderr
#   -h, --help         display this help text and exit
#   --version          display version information and exit
#
# Exit status:
#   By default returns 0. If return code is set, then returns it.
#
# Reads variables configuration from following files (if they exist),
# in this order:
# "$HOME/.finishedrc", "$HOME/.config/finished/.finishedrc" and "./.finishedrc".
#
# Examples:
# $ finished.sh Message to display
# $ finished.sh "Message: $PWD"
# $ sleep 3; finished.sh --sound message-new-email --return $? Message
# $ wrong; finished.sh --sound message-new-email --return $? Message
#
# Tips:
# * Define this alias to transmit the return code from the previous command:
#   $ alias finished='finished.sh --return $?'
# * And then use it like that:
#   $ sleep 3; finished Message
#   $ wrong; finished Message
#
#
# Requirement: notify-send
#
#
# Latest version and more explanations on Bitbucket:
# https://bitbucket.org/OPiMedia/etoolbox/src/master/finished/
#
# GPLv3 --- Copyright (C) 2021, 2023 Olivier Pirson
# http://www.opimedia.be/
##
# * November 19, 2023: Few cleaning.
# * November 12, 2023: Few cleaning.
# * November 14, 2021: Cleaned IFS.
# * November 4, 2021: Corrected typos.
# * October 31, 2021:
#   - Added --transient and ---debug-options options.
#   - Encapsulated local variables and function to main function.
# * October 30, 2021: Set global constants as read only.
# * October 27, 2021: Added --i-ok and --sound-ok options.
# * Started October 23, 2021: First public version.
#

unset RETURN_CODE


# Main function
function finished {
    # shellcheck disable=SC2155
    local -r script_name=$(basename "$0")

    local -i debug_options=1  # 0 is for true, other is for false


    # Prints text_in_red and text in the error output.
    function error_msg {  # text_in_red, text
        echo "$(tput setaf 1)$1$(tput sgr0) $2" 1>&2
    }

    # Prints "$2", the next argument given.
    # If not available, call print_help_exit "$1".
    function next_argument {  # [argument1 ...]
        if [[ $# -lt 2 ]]; then  # missing argument
            print_help_exit "$1"
        fi
        printf '%s' "$2"
    }

    # Prints help message (reads from the script itself) and quits.
    # If called without argument,
    # then prints to stdout and exit 0,
    # else prints to stderr and exit 1.
    function print_help_exit {  # [wrong option]
        local -r wrong_option="$1"
        local -i return_code=0

        if [[ $# -ne 0 ]]; then
            return_code=1
            exec 1>&2
            error_msg 'Unknown or wrong option!' "\"$wrong_option\""
        fi

        local -i skip_begin=0

        local line
        while IFS='' read -r line; do  # prints the header comment until the line ##
            if [[ "$line" == '# Usage:'* ]]; then skip_begin=1; fi
            if [[ "$skip_begin" -eq 0 ]]; then continue; fi
            if [[ "$line" == '##'* ]]; then break; fi
            line="${line:2}"
            printf '%s\n' "${line//finished.sh/$script_name}"
        done < "${BASH_SOURCE[0]}"

        echo "Current version: $(print_version)"

        exit $return_code
    }

    # Prints all arguments on stderr, surrounded by [].
    function print_options {  # [option1 ...]
        {
            if [[ $debug_options -eq 0 ]]; then
                echo -n 'All command line options passed to notify-send:'
            else
                echo -n 'Following command line options:'
            fi
            while [[ $# -ne 0 ]]; do
                echo -n " [$1]"
                shift
            done;
            echo
        } 1>&2
    }

    # Prints version (reads it from the script itself, just after the first ## line).
    function print_version {
        local -i skip_begin=0
        local version

        local line
        while IFS='' read -r line; do
            if [[ "$line" == '##'* ]]; then skip_begin=1; fi
            if [[ "$skip_begin" -eq 0 ]]; then continue; fi
            line="${line:4}"
            if [[ -n "$line" ]]; then
                IFS=':' read -r version line <<< "$line"
                printf '%s\n' "$version"

                break
            fi
        done < "${BASH_SOURCE[0]}"
    }


    local icon
    local icon_error
    local icon_ok
    local level
    local sound
    local sound_error
    local sound_ok

    local -i beep=1
    local body=''
    local -i display_date=1
    local -i read_config=0
    local summary='FINISHED'
    local -i time=10000

    # Check if --no-config option
    local option
    for option in "$@"; do
        if [[ "$option" == '--no-config' ]]; then
            read_config=1

            break
        fi
    done

    if [[ $read_config -eq 0 ]]; then
        # Reads configuration files
        local file
        for file in "$HOME/.finished.config" "$HOME/.config/finished/.finished.config" "./.finished.config"; do
            if [[ -f "$file" ]]; then
                # shellcheck disable=SC1090
                source "$file"
            fi
        done
    fi

    # Reads parameters
    declare -a notify_send_options

    while [[ $# -ne 0 ]]; do
        case "$1" in
            --beep)
                beep=0
                ;;
            --date)
                display_date=0
                ;;
            ---debug-options)
                print_options "$@"
                debug_options=0
                ;;
            -h|--help)
                print_help_exit
                ;;
            -i)
                icon=$(next_argument "$@") || exit $?
                shift
                ;;
            --i-err)
                icon_error=$(next_argument "$@") || exit $?
                shift
                ;;
            --i-ok)
                icon_ok=$(next_argument "$@") || exit $?
                shift
                ;;
            --no-config)
                # Already used
                ;;
            --return)
                RETURN_CODE=$(next_argument "$@") || exit $?
                shift
                ;;
            --summary)
                summary=$(next_argument "$@") || exit $?
                shift
                ;;
            --sound)
                sound=$(next_argument "$@") || exit $?
                shift
                ;;
            --sound-err)
                sound_error=$(next_argument "$@") || exit $?
                shift
                ;;
            --sound-ok)
                sound_ok=$(next_argument "$@") || exit $?
                shift
                ;;
            -t)
                time=$(next_argument "$@") || exit $?
                shift
                ;;
            --transient)
                notify_send_options+=('--hint' 'int:transient:1')
                ;;
            -u)
                level=$(next_argument "$@") || exit $?
                shift
                ;;
            --version)
                echo "finished.sh ($(print_version))"

                exit 0
                ;;
            -*)  # unknown parameter
                print_help_exit "$1"
                ;;
            *)
                if [[ -z "$body" ]]; then
                    body="$1"
                else
                    body="$body $1"
                fi
                ;;
        esac
        shift
    done

    # Builds message and options to sent to notify-send command
    if [[ display_date -eq 0 ]]; then
        summary="$summary $(date)"
    fi

    if [[ -n "$RETURN_CODE" ]]; then
        summary="[$RETURN_CODE] $summary"

        if [[ $RETURN_CODE -eq 0 ]]; then
            if [[ -n "$icon_ok" ]]; then
                icon="$icon_ok"
            fi
            if [[ -n "$sound_ok" ]]; then
                sound="$sound_ok"
            fi
        else
            if [[ -n "$icon_error" ]]; then
                icon="$icon_error"
            fi
            if [[ -n "$sound_error" ]]; then
                sound="$sound_error"
            fi
        fi
    fi

    if [[ -n "$icon" ]]; then
        notify_send_options+=('-i' "$icon")
    fi

    if [[ -n "$level" ]]; then
        notify_send_options+=('-u' "$level")
    fi

    if [[ -n "$sound" ]]; then
        notify_send_options+=("--hint=string:sound-name:$sound")
    fi

    if [[ -n "$time" ]]; then
        notify_send_options+=('-t' "$time")
    fi

    # Sends notifications and beep
    if [[ $debug_options -eq 0 ]]; then
        print_options "${notify_send_options[@]}" "$summary" "$body"
    fi
    notify-send "${notify_send_options[@]}" "$summary" "$body" || echo 'notify-send failed!' 1>&2

    if [[ $beep -eq 0 ]]; then
        echo -n -e '\a'
    fi
}


finished "$@"

if [[ -n "$RETURN_CODE" ]]; then
    exit "$RETURN_CODE"
fi

exit 0
