#!/bin/bash

#
# Usage: test_printargs.sh
#
# December 28, 2023
#
# Apache License 2.0 --- Copyright (C) 2023 Olivier Pirson
# http://www.opimedia.be/
#

OUTPUT="$PWD/test_printargs_output.txt"

{  # Write stdout and stderr to "$OUTPUT"
    echo "Test results of printargs --- $(date)"


    # ShellCheck
    echo '$ shellcheck ../printargs'
    shellcheck ../printargs

    echo '$ shellcheck --shell ksh ../printargs'
    shellcheck --shell ksh ../printargs

    echo '$ shellcheck --shell bash ../printargs'
    shellcheck --shell bash ../printargs

    echo '$ shellcheck test_printargs.sh'
    shellcheck test_printargs.sh


    declare -a OPTIONS_LIST

    OPTIONS_LIST=('' 'one' 'one two')

    # Execute the script directly
    echo '----- sh - script called -----'
    for OPTIONS in "${OPTIONS_LIST[@]}"; do
        echo "-- Args: $OPTIONS --"
        # shellcheck disable=SC2086  # double quotes
        ../printargs $OPTIONS
    done
    echo "-- Args: 'one' 'two blablabla' 'three' --"
    ../printargs 'one' 'two blablabla' 'three'


    # Source the script in each possible shell
    for SHELL in sh ksh bash zsh; do
        echo "----- $SHELL - script sourced - function called -----"
        for OPTIONS in "${OPTIONS_LIST[@]}"; do
            echo "-- Args: $OPTIONS --"
            # shellcheck disable=SC2086  # double quotes
            echo ". ../printargs
printargs $OPTIONS" | $SHELL
        done
        echo "-- Args: 'one' 'two blablabla' 'three' --"
        echo ". ../printargs
printargs 'one' 'two blablabla' 'three'" | $SHELL
    done

    echo '---------- END ----------'
} 2>&1 | tee "$OUTPUT"


# Compare results
echo "COMPARE $OUTPUT and test_printargs_correct_output.txt"
if diff --color <(tail --lines +2 "$OUTPUT") <(tail --lines +2 test_printargs_correct_output.txt); then
    echo "$(tput setaf 2)OK$(tput sgr0)"
else
    echo "$(tput setaf 1)DIFFERENT$(tput sgr0)"
fi
