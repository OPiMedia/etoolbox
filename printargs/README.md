# printargs
Silly `sh` script that just prints all its arguments, surrounded by brackets.
Really silly, but can help debugging other scripts.



## Use directly the `sh` script, from any shell

Put the script `printargs` somewhere accessible in your `PATH`
and then run:

```bash
$ printargs one 'two blablabla' three
```

That prints:

```
0 [<path>/printargs]
1 [one]
2 [two blablabla]
3 [three]
```



## Source the script in compatible shell and use the function
Install the `printargs` function in your shell by running
(the script `printargs` need to be accessible in your `PATH`)

```sh
. printargs
```

or

```bash
source printargs
```

Do it in `.shrc`, `.kshrc`, `.bashrc` or `.zshrc` configuration files,
depending of your shell,
to permanently install the function.

Then run:

```bash
$ printargs one 'two blablabla' three
```

That prints:

```
1 [one]
2 [two blablabla]
3 [three]
```

That works at least with `sh`, `ksh`, `bash` and `zsh`.


## Usefulness
The purpose is to print arguments passed to a script or a function,
to check their content during a debugging step.
Like this:

```bash
function foo {
    printargs "$@"
    <...>
}
```



## Author: 🌳 Olivier Pirson — OPi ![OPi][OPi] 🇧🇪🇫🇷🇬🇧 🐧 👨‍💻 👨‍🔬
🌐 Website: <http://www.opimedia.be/>

💾 Bitbucket: <https://bitbucket.org/OPiMedia/workspace/repositories>

- 📧 <olivier.pirson.opi@gmail.com>
- 🧑‍🤝‍🧑 Bluesky: <https://bsky.app/profile/opimedia.bsky.social> — Mastodon: <https://mamot.fr/@OPiMedia> — X/Twitter: <https://twitter.com/OPirson>
- 👨‍💻 LinkedIn: https://www.linkedin.com/in/olivierpirson/ — CV: http://www.opimedia.be/CV/English.html
- other profiles: <http://www.opimedia.be/about/>

[OPi]: http://www.opimedia.be/_png/OPi.png



## License: [Apache License 2.0](https://www.apache.org/licenses/LICENSE-2.0)
Copyright 2023 Olivier Pirson

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

https://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.



## Changes
* December 11, 2023: First public version.
* Started December 10, 2023

#printargs #shell #sh #ksh #bash #zsh
